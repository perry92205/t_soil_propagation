from namelist import *

def process_param ():
	params = {}
	global_vars = globals()
	for var_name in global_vars:
		if not var_name.startswith("_") and var_name is not "process_param":
			var_value = global_vars.get (var_name)
			if var_name == "case_name":
				var_value = "archieve/" + var_value
			params [var_name] = var_value
	return params
