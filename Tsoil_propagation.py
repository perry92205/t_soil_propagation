import numpy as np
from netCDF4 import Dataset
import os
import time_evolution

def write_one_ens_var (fname, var_tsoil, zlevel, timestamp):
	[tyr, tmon, tday, min1, min2] = timestamp

	os.system ("rm %s" %(fname))
	with Dataset (fname, "w") as fout:
		time = fout.createDimension ("time", None)
		times = fout.createVariable ("time", "i4", ("time", ))
		times.units = "id of min on %04d-%02d-%02d" %(tyr, tmon, tday)
		times [:] = np.arange (min1+1, min2+1)

		z = fout.createDimension ("z", zlevel.shape [0])
		zs = fout.createVariable ("z", "i4", ("z", ))
		zs.units = "cm"
		zs [:] = zlevel

		var = fout.createVariable ("T_soil", "f4", ("time", "z"))
		var.units = "oC"
		var [:] = var_tsoil

def write_n_ens_var (fname, var_tsoil, zlevel, timestamp, n_ens: int):
	[tyr, tmon, tday, min1, min2] = timestamp

	os.system ("rm %s" %(fname))
	with Dataset (fname, "w") as fout:
		nens = fout.createDimension ("ensid", n_ens)
		nenss = fout.createVariable ("ensid", "i4", ("ensid", ))
		nenss.units = "unitless"
		nenss [:] = np.arange (1, n_ens+1)

		time = fout.createDimension ("time", None)
		times = fout.createVariable ("time", "i4", ("time", ))
		times.units = "id of min on %04d-%02d-%02d" %(tyr, tmon, tday)
		times [:] = np.arange (min1+1, min2+1)

		z = fout.createDimension ("z", zlevel.shape [0])
		zs = fout.createVariable ("z", "i4", ("z", ))
		zs.units = "cm"
		zs [:] = zlevel

		var = fout.createVariable ("T_soil", "f4", ("ensid", "time", "z"))
		var.units = "oC"
		var [:] = var_tsoil

def write_out_var (casename, var_tsoil, zlevel, timestamp, is_DA: bool = False, output_type: str = "background", n_ens = 1):
	if is_DA:
		if output_type == "background":
			# not yet DA
			pass
		elif output_type == "analysis":
			#DA is done
			pass
		else:
			print ("output_type: \"background\" or \"analysis\"")
			exit ()
	else:
		# normal case
		var_ens = var_tsoil.shape [0]
		[tyr, tmon, tday, min1, min2] = timestamp
		timename = "%04d-%02d-%02d_%04d-%04d" %(tyr, tmon, tday, min1+1, min2)
		if var_ens == 1:
			var_tsoil = var_tsoil [0, :, :]
			fname = "archieve/%s/%s-%s.nc" %(casename, casename, timename)
			write_one_ens_var (fname, var_tsoil, zlevel, timestamp)
		else:
			fname = "archieve/%s/%s-%s_%03d_ens.nc" %(casename, casename, timename, var_ens)
			write_one_ens_var (fname, var_tsoil, zlevel, timestamp, var_ens)

#		os.system ("rm %s" %(fname))
#		with Dataset (fname, "w") as fout:
#			time = fout.createDimension ("time", None)
#			times = fout.createVariable ("time", "i4", ("time", ))
#			times.units = "id of min on %04d-%02d-%02d" %(tyr, tmon, tday)
#			times [:] = np.arange (min1+1, min2+1)
#
#			z = fout.createDimension ("z", zlevel.shape [0])
#			zs = fout.createVariable ("z", "i4", ("z", ))
#			zs.units = "cm"
#			zs [:] = zlevel
#
#			var = fout.createVariable ("T_soil", "f4", ("time", "z"))
#			var.units = "oC"
#			var [:] = var_tsoil

def get_markov_matrix (markov_params, dt, z):
	# z must have level 0 (i.e., surface layer)
	kappa = markov_params ["kappa"]
	cv = markov_params ["cv"]

	nz = z.shape [0]
	dz = (z [0:nz-1] - z [1:nz])/100 # units: m

	nz = nz - 1 # now, nz is for matrix shape
	M = (kappa / cv) * dt / (dz ** 2)
	markov = np.zeros ((nz, nz), dtype = float)

	# level 1
	m = M[0]
	markov [0, 0:2] = [1+3*m, -m]
	for iz in range (1, nz-1):
		m = M [iz]
		markov [iz, iz-1:iz+2] = [-m, 1+2*m, -m]
	# the last level
	m = M[-1]
	markov [nz-1, nz-2:nz] = [-m, 1+m]

	return markov

def Tsoil_propagation (casename, markov_params, dt, var_all, begin_time, end_time, dt_output = 1440, dt_da = 30, is_DA = False, n_ens:int = 1) -> float:
	# initialize some constant
	min_of_day = 24 * 60
	day_of_mon = np.array ([31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31])

	'''
	# Check variables
	'''
	# check dt* settings
	if min_of_day % dt_output != 0:
		print ("Set output frquency be part of 1440")
		exit ()
	if min_of_day % dt_da != 0:
		print ("Set da frquency be part of 1440")
		exit ()
	if n_ens <= 0:
		print ("Ensemble number should be larger than 0!!")
		exit ()

	# check dimensions of all variables
	z = var_all ["zlevel"]
	T_IC = var_all ["T_IC"]
	T0 = var_all ["T0"]
	if is_DA == False and n_ens == 1:
		# default settings
		T_IC = T_IC.reshape ((1, -1))
		T0 = T0.reshape ((1, -1))
	else:
		if (n_ens, z.shape[0]) != T_IC.shape:
			print ("Dimensions of ensemble and zlevel are not equal")
			exit ()

	noutput = int (min_of_day // dt_output)
	nda = int (min_of_day // dt_da)
	if noutput < nda:
		print ("# times of DA should <= output freqnecy")
		exit ()

	'''
	# Initialization
	'''
	# initialize some variables
	verbose = True # temporary use
	t_start = T_IC [1:]
	[begin_year, begin_mon, begin_day] = begin_time
	[end_year, end_mon, end_day] = end_time

	'''
	# cacluate Markov matrix
	'''
	markov = get_markov_matrix (markov_params, dt, z)
	markov_inv = np.linalg.pinv (markov)
	M = -markov [0, 1]

	flag = 0
	IC = T_IC
	for tyr in range (begin_year, end_year+1):
		# set correct month range
		[tmon1, tmon2] = time_evolution.get_month_range ([begin_year, begin_mon], [end_year, end_mon], tyr)
		# ------
		for tmon in range (tmon1, tmon2+1):
			# set correct day range
			[tday1, tday2] = time_evolution.get_day_range ([begin_year, begin_mon, begin_day], [end_year, end_mon, end_day], [tyr, tmon])
			# ------
			for tday in range (tday1, tday2+1):
				if verbose:
					print ("Current time: %04d-%02d-%02d" %(tyr, tmon, tday))
				# Initialize every day
				new_IC = np.zeros ((n_ens, T_IC.shape[1]), dtype = float)
				output_count, da_count = 0, 0
				output_t1, output_t2 = 0, 0
				minute_clock = 0
				t_background = np.zeros ((n_ens, min_of_day, z.shape [0]), dtype = float)

				if minute_clock == 0:
					# initialize the first step
					t_background [:, 0, :] = IC
				for tmin in range (0, min_of_day):
					for iens in range (n_ens):
						'''
						# process each ensemble member
						# Ay = b, we want to calculate y
						# hence y = A^-1 * b
						'''
						# setup the matrix
						b = t_background [iens, tmin, 1:] * 1
						b [0] += 2 * M * T0[iens, flag]
						# follow the equation
						y = np.dot (markov_inv, b)

						if tmin+1 ==  min_of_day:
							# end of day, this data is for 00:00 tomorrow
							# so back up the y for each ensemble
							new_IC [iens, 1:] = y
							new_IC [iens, 0] = T0 [iens, flag]
						else:
							t_background [iens, tmin+1, 1:] = y
							t_background [iens, tmin+1, 0] = T0 [iens, flag]
					# increase each minute
					output_count += 1
					da_count += 1
					minute_clock += 1
					flag += 1
					# other process when dt_* is matched within a day
					if da_count == dt_da and is_DA:
						# TODO: process da
						da_count = 0
						pass
					if output_count == dt_output:
						output_t2 = tmin + 1
						write_out_var (casename, t_background [:, output_t1:output_t2, :], z, [tyr, tmon, tday, output_t1, output_t2])
						output_t1 = output_t2
						output_count = 0
				# the final step of a day
				IC = new_IC
				del (new_IC)

