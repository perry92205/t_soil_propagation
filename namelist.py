
kappa = 3
cv = 4e+6
is_DA = False
dt = 1
dt_output = 30
dt_da = 30
case_name = "try1"
begin_year = 2018
begin_mon = 5
begin_day = 1
end_year = 2018
end_mon = 5
end_day = 31
observation = "data/nc_data/T_soil_interpolate.nc"
