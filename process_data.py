import numpy as np
from netCDF4 import Dataset

def get_all_observation (file_path):
	try:
		with Dataset (file_path, "r") as f:
			time1 = f.variables ["time1"][:]
			time2 = f.variables ["time2"][:]
			zlevel = f.variables ["z"][:]
			tsoil = f.variables ["T_soil"][:]
	except:
		print ("No scuh file: ", file_path)
		exit ()
	return time1, time2, zlevel, tsoil

def get_daily_output_data (casename, current_time, dt):
	print (current_time)
	noutput = int (24 * 60 // dt)
	[tyr, tmon, tday] = current_time
	var_out = []
	for ioutput in range (noutput):
		t1, t2 = ioutput * dt + 1, (ioutput + 1) * dt
		with Dataset ("archieve/%s/%s-%04d-%02d-%02d_%04d-%04d.nc" %(casename, casename, tyr, tmon, tday, t1, t2), "r") as f:
			var_out.append (f.variables ["T_soil"][:])
	var_out = np.ma.array (var_out)
	[nout, nmin, nlev] = var_out.shape
	return np.ma.array (var_out).reshape ((nout * nmin, nlev))

def get_zlevel (casename, current_time, dt):
	[tyr, tmon, tday] = current_time
	t1, t2 = 1, dt
	with Dataset ("archieve/%s/%s-%04d-%02d-%02d_%04d-%04d.nc" %(casename, casename, tyr, tmon, tday, t1, t2), "r") as f:
		zlevel = f.variables ["z"][:]
	return zlevel

def get_all_output (casename, dt, begin_time, end_time):
	[begin_year, begin_mon, begin_day] = begin_time
	[end_year, end_mon, end_day] = end_time
	day_of_mon = np.array ([31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31])

	var_out = []
	zlevel_get = 0
	for tyr in range (begin_year, end_year+1):
		if begin_year == end_year:
			tmon1, tmon2 = begin_mon, end_mon
		elif tyr == begin_year:
			tmon1, tmon2 = begin_mon, 12
		elif tyr == end_year:
			tmon1, tmon2 = 1, end_mon
		else:
			tmon1, tmon2 = 1, 12

		for tmon in range (tmon1, tmon2+1):
			if tyr == begin_year and tmon == begin_mon:
				tday1, tday2 = begin_day, day_of_mon [tmon-1]
			elif tyr == end_year and tmon == end_mon:
				tday1, tday2 = 1, end_day
			else:
				tday1, tday2 = 1, day_of_mon [tmon-1]

			for tday in range (tday1, tday2+1):
				var_out.append (get_daily_output_data (casename, [tyr, tmon, tday], dt))
	zlevel = get_zlevel (casename, [tyr, tmon, tday], dt)
	return [np.ma.array (var_out), zlevel]
