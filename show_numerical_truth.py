import numpy as np
from netCDF4 import Dataset
import process_param
import process_data

import matplotlib.pyplot as plt

'''
# get parameters
'''
params = process_param.process_param ()
begin_time = [params.get ("begin_year", 2018), params.get ("begin_mon", 1), params.get ("begin_day", 1)]
end_time = [params.get ("end_year", 2018), params.get ("end_mon", 1), params.get ("end_day", 1)]
dt_output = params.get ("dt_output", 1440)
noutput_a_day = 24 * 60 /dt_output 

'''
# read from case
'''
casename = "numerical_truth"
caseroot = "archieve/%s" %casename
[tsoil_num_all, zlevel_num_all] = process_data.get_all_output (casename, dt_output, begin_time, end_time)

# select levels you want
select_zlevel = [0, 1, 2, 4, 10, 20]
tsoil_num = tsoil_num_all [:, :, select_zlevel]
zlevel_num = zlevel_num_all [select_zlevel]

'''
# read from observaion
'''
src = "data/nc_data"
file_name = "T_soil.nc"
with Dataset (src + "/" + file_name, "r") as f:
	tsoil_obs = f.variables ["T_soil"][:]
	zlevel_obs = f.variables ["z"][:]

[ndays, nmin, nlev] = tsoil_num.shape

'''
# plot climatology
'''
tsoil_num_climate_daily = np.ma.mean (tsoil_num, axis = 0)
tsoil_obs_climate_daily = np.ma.mean (tsoil_obs, axis = 0)

color_name = ["r-", "b-"]
timeindex = np.arange (1, nmin+1)
fig, axes = plt.subplots (nrows = nlev, sharex = True, figsize = (8, 12))
for ilev in range (nlev):
	ax = axes [ilev]
	ax.plot (timeindex, tsoil_num_climate_daily [:, ilev], color_name [0])
	ax.plot (timeindex, tsoil_obs_climate_daily [:, ilev], color_name [1])
	ax.set_ylabel ("T_soil (oC)", fontsize = 14)
	ax.set_title ("at level %d (cm)" %(-zlevel_obs [ilev]))
plt.savefig ("output/daily_climatology_numerical_truth_a_mon.png")
plt.close ()
