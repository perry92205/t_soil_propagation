
import process_param
import process_data
import Tsoil_propagation as propagation
import time
import numpy as np
from netCDF4 import Dataset

def create_case (casename):
	import os
	os.system ("mkdir -p %s" %casename)

def main (params):
	casename = "numerical_truth"
	caseroot = "archieve/%s" %casename
	create_case (caseroot)

	kappa = params.get ("kappa", 0.2)
	cv = params.get ("cv", 1e+5)
	dt = params.get ("dt", 1.) # units: 1min
	# z read from nc file
	time1, time2, zlevel, tsoil = process_data.get_all_observation (params.get ("observation", None))

	T_IC = tsoil [0, 0, :]
	T0 = tsoil [:, :, 0].reshape ((time1.shape [0] * time2.shape[0], 1))
	begin_time = [params.get ("begin_year", 2018), params.get ("begin_mon", 5), params.get ("begin_day", 1)]
	end_time = [params.get ("end_year", 2018), params.get ("end_mon", 1), params.get ("end_day", 1)]
	dt_output = params.get ("dt_output", 86400)
	dt_da = params.get ("dt_da", 30)

	var_all = {}
	var_all ["T_IC"] = T_IC
	var_all ["T0"] = T0
	var_all ["zlevel"] = zlevel

	markov_params = {}
	markov_params ["kappa"] = kappa
	markov_params ["cv"] = cv
	propagation.Tsoil_propagation (casename, markov_params, dt, var_all, begin_time, end_time, dt_output, dt_da, is_DA = False)


if __name__ == "__main__":
	params = process_param.process_param ()
	main (params)
