import process_data
import numpy as np
from netCDF4 import Dataset
import os

def calculate_nmissing (previous_time, current_time):
	[previous_hr, previous_min] = previous_time
	[current_hr, current_min] = current_time
	count = 0
	if previous_hr == -1 and previous_min == -1:
		previous_hr, previous_min = 0, -1
		if previous_hr == current_hr:
			count = current_min - previous_min - 1
			return count

	if previous_hr == current_hr and previous_min > -1:
		count = current_min - previous_min - 1
	else:
		for thr in range (previous_hr, current_hr+1):
			if thr < current_hr:
				if thr == previous_hr:
					count += 60 - previous_min - 1
				else:
					count += 60
			else:
				count += current_min
	if current_hr == 23 and current_min == 59:
		count += 1
	return count

def extract_data (line, missing):
	info_data = []
	info_data_mask = []
	try:
		for idata in range (len (line)):
			if line [idata].rstrip () == "":
				info_data.append (missing)
				info_data_mask.append (True)
			elif line [idata].endswith ("*"):
				info_data.append (missing)
				info_data_mask.append (True)
			else:
				info_data.append (float (line [idata]))
				info_data_mask.append (False)
	except:
		info_data = []
		info_data_mask = []
		for idata in range (len (line)):
			info_data.append (missing)
			info_data_mask.append (True)

	if len (line) != 58:
		# # of data are not correct
		info_data = []
		info_data_mask = []
		for idata in range (58):
			info_data.append (missing)
			info_data_mask.append (True)
	return np.ma.array (info_data), np.array (info_data_mask)

def get_daily_data (file_path, missing = -99.99):
	ndata = 24 * 60
	nkind = 59 - 1 # [0]: time record
	previous_hr, previous_min = -1, -1

	info_data = np.ma.zeros ((ndata, nkind), dtype = float)
	info_data [:, :] = missing
	info_data_mask = np.zeros ((ndata, nkind), dtype = bool)
	info_time = []

	try:
		with open (file_path, "r") as f:
			lines = f.readlines()
	except:
		for i in range (ndata): info_time.append (-1)
		info_data_mask [:] = True
		return (info_time, np.ma.array (info_data, mask = info_data_mask))

	current = 0
	for line in lines:
		line = line.rstrip ().split (",")
		# check time
		time_point = int (float (line [0]) % 1E+4)
		if time_point == 2400 and previous_hr == -1:
			previous_hr, previous_min = 0, 0
			info_time.append ([0, 0])
			info_data [current, :], info_data_mask [current, :] = extract_data (line [1:], missing)
			current += 1
		else:
			current_hr, current_min = int (time_point // 1E+2), int (time_point%1E+2)
			if current_hr == 24 and current_min == 0:
				break
			if current_min - previous_min == 1:
				# next min in same hr
				pass
			elif previous_min == 59 and current_min == 0:
				pass
			elif current_hr == 23 and current_min == 59 and previous_hr == -1:
				# May be yesterday data, but not sure
				continue
			else:
				nmissing = calculate_nmissing ([previous_hr, previous_min], [current_hr, current_min])
				info_data_mask [current:current+nmissing, :] = True
				current += nmissing
			info_data [current, :], info_data_mask [current, :] = extract_data (line [1:], missing)
			previous_hr, previous_min = current_hr, current_min
			current += 1
	if previous_hr != 23 or previous_min != 59:
		nmissing = calculate_nmissing ([previous_hr, previous_min], [23, 59])
		info_data_mask [current:current+nmissing, :] = True
		current += nmissing
	return (info_time, np.ma.array (info_data, mask = info_data_mask))

'''
user define
'''
raw_data_source = "data/raw_data/Grassland_MN_data"
begin_year, begin_mon, begin_day = 2018, 5, 1
end_year, end_mon, end_day = 2019, 4, 30

'''
work section
'''

day_of_mon = np.array ([31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31])
missing = -99.99
total_data = []
for tyr in range (begin_year, end_year+1):
	if tyr < end_year:
		m1, m2 = begin_mon, 12
	elif begin_year == end_year:
		m1, m2 = begin_mon, end_mon
	else:
		m1, m2 = 1, end_mon

	for tmon in range (m1, m2+1):
		if tyr == begin_year and tmon == begin_mon:
			t1, t2 = begin_day, day_of_mon [tmon-1]
		elif tyr == end_year and tmon == end_mon:
			t1, t2 = 1, end_day
		else:
			t1, t2 = 1, day_of_mon [tmon-1]
		for tday in range (t1, t2+1):
			file_name = "MN_%04d%02d%02d.txt" %(tyr, tmon, tday)
			print ("\t", file_name)
			(info_time, data) = get_daily_data (raw_data_source + "/" + file_name, missing = missing)
			total_data.append (data)
total_data = np.ma.array (total_data)

t_soil_index = np.array ([53, 54, 55, 56, 58, 59]) - 2
t_soil = total_data [:, :, t_soil_index]
zlevel = -np.array ([0, 5, 10, 20, 50, 100])
# extract data
print (t_soil.shape)

file_name = "data/nc_data/T_soil.nc"
os.system ("rm -f %s" %file_name)
with Dataset (file_name, "w") as fout:
	time1 = fout.createDimension ("time1", None)
	time1s = fout.createVariable ("time1", "i4", ("time1", ))
	time1s.units = "doy of month begin 2018/5/1"
	time1s [:] = np.arange (1, t_soil.shape [0]+1)

	time2 = fout.createDimension ("time2", 1440)
	time2s = fout.createVariable ("time2", "i4", ("time2", ))
	time2s.units = "minute of each day"
	time2s [:] = np.arange (1, 1440+1)

	z = fout.createDimension ("z", t_soil_index.shape [0])
	zs = fout.createVariable ("z", "i4", ("z", ))
	zs.units = "cm"
	zs [:] = zlevel

	var = fout.createVariable ("T_soil", "f4", ("time1", "time2", "z"), fill_value = missing)
	var.units = "oC"
	var [:] = t_soil

