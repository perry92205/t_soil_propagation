import numpy as np
from netCDF4 import Dataset
import os


def interpolate_missing_data (var, begin_mon = 1):
	day_of_mon = np.array ([31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31])
	(ndays, nmin, nlev) = var.shape
	var_out = np.zeros (var.shape, dtype = var.dtype)
	var_out_mask = np.zeros (var.shape, dtype = bool)
	var_out_mask [:] = False

	current, current_mon = 0, begin_mon-1
	while current < ndays:
		nday_of_mon = day_of_mon [current_mon]
		t1, t2 = current, current + nday_of_mon
		perturb = np.random.normal (0, 1, nday_of_mon).reshape ((nday_of_mon, 1, 1))
		monthly_mean = np.ma.mean (var [t1:t2, :, :], axis = 0, keepdims = True)
		monthly_std = np.ma.std (var [t1:t2, :, :], axis = 0, keepdims = True)

		componset_missing = monthly_mean + perturb * monthly_std

		var_out [t1:t2, :, :] = np.where (np.ma.getmaskarray (var [t1:t2, :, :]) == False, var [t1:t2, :, :], componset_missing)
		current += nday_of_mon
		current_mon = (current_mon+1) % 12

	return np.ma.array (var_out, mask = var_out_mask)

missing = -99.99
file_name = "data/nc_data/T_soil.nc"
with Dataset (file_name, "r") as f:
	ttime1, ttime2 = f.variables ["time1"][:], f.variables ["time2"][:]
	zlevel = f.variables ["z"][:]
	tsoil = f.variables ["T_soil"][:]

znew = -np.arange (0, 100+1, 5)
tsoil_new = np.ma.zeros ((ttime1.shape[0], ttime2.shape[0], znew.shape [0]), dtype = tsoil.dtype)

for iz in range (znew.shape [0]):
	z = znew [iz]
	if z in zlevel:
		ind = np.where (zlevel == z) [0][0]
		tsoil_new [:, :, iz] = tsoil [:, :, ind]
	else:
		ind1 = np.where (z < zlevel)[0][-1]
		ind2 = np.where (z > zlevel)[0][0]
		tmp1, tmp2 = tsoil [:, :, ind1], tsoil [:, :, ind2]
		tsoil_new [:, :, iz] = (z - zlevel [ind1]) * (tmp2 - tmp1) / (zlevel [ind2] - zlevel [ind1]) + tmp1

tsoil_new = interpolate_missing_data (tsoil_new, begin_mon = 5)

file_name = "data/nc_data/T_soil_interpolate.nc"
os.system ("rm -f %s" %file_name)
with Dataset (file_name, "w") as fout:
	time1 = fout.createDimension ("time1", None)
	time1s = fout.createVariable ("time1", "i4", ("time1", ))
	time1s.units = "doy of month begin 2018/5/1"
	time1s [:] = ttime1

	time2 = fout.createDimension ("time2", 1440)
	time2s = fout.createVariable ("time2", "i4", ("time2", ))
	time2s.units = "minute of each day"
	time2s [:] = ttime2 

	z = fout.createDimension ("z", znew.shape [0])
	zs = fout.createVariable ("z", "i4", ("z", ))
	zs.units = "cm"
	zs [:] = znew

	var = fout.createVariable ("T_soil", "f4", ("time1", "time2", "z"), fill_value = missing)
	var.units = "oC"
	var [:] = tsoil_new


