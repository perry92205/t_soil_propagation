def get_month_range (begin_time, end_time, current_time):
	[begin_year, begin_mon] = begin_time
	[end_year, end_mon] = end_time
	tyr = current_time

	if begin_year == end_year:
		tmon1, tmon2 = begin_mon, end_mon
	elif tyr == begin_year:
		tmon1, tmon2 = begin_mon, 12
	elif tyr == end_year:
		tmon1, tmon2 = 1, end_mon
	else:
		tmon1, tmon2 = 1, 12
	
	return [tmon1, tmon2]

def get_day_range (begin_time, end_time, current_time):
	import numpy as np
	[begin_year, begin_mon, begin_day] = begin_time
	[end_year, end_mon, end_day] = end_time
	[tyr, tmon] = current_time

	day_of_mon = np.array ([31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31])

	if tyr == begin_year and tmon == begin_mon:
		if begin_year == end_year:
			# in the same year
			if begin_mon == end_mon:
				# in the same month
				tday1, tday2 = begin_day, end_day
		else:
			tday1, tday2 = begin_day, day_of_mon [tmon-1]
	elif tyr == end_year and tmon == end_mon:
		tday1, tday2 = 1, end_day
	else:
		tday1, tday2 = 1, day_of_mon [tmon-1]
	
	return [tday1, tday2]
